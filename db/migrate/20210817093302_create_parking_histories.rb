class CreateParkingHistories < ActiveRecord::Migration
  def change
    create_table :parking_histories do |t|

      t.timestamps null: false
    end
  end
end
