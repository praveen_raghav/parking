class CreateParkingSpaces < ActiveRecord::Migration
  def change
    create_table :parking_spaces do |t|

      t.timestamps null: false
    end
  end
end
