class ParkingHistoriesController < ApplicationController

  def show_all_parking_status
    # render json: ParkingHistoryBlueprint.render_as_hash(ParkingSpace.all, {view: :show_all_parking_status})
    render json: ParkingSpace.all
  end

  def park_vehicle
    parking_space = ParkingSpace.where({spece_type: params[:vehicle_type], is_occupied: false}).first
    if parking_space.present?
      parking_history = nil
      ActiveRecord::Base.transaction do
        parking_space.is_occupied = true
        parking_space.save

        parking_history = ParkingHistory.new
        parking_history.user_id = User.find(params[:vehicle_number]).id
        parking_history.parking_space_id = parking_space.id
        parking_history.start_time = Time.now.utc
        parking_history.save
      end

      render json: parking_history.reload
      return
    else
      render json: {status: 'parking is full'}
      return
    end
  end

  def take_vehicle
    parking_history = ParkingHistory.find(params[:parking_history_id])
    ActiveRecord::Base.transaction do
      parking_history.end_time =  Time.now.utc
      parking_history.save

      parking_space = ParkingSpace.find(parking_history.parking_space_id)
      parking_space.is_occupied = false
      parking_space.save
    end
    render json: {bill_amount: calculate_bill(parking_history.start_time)}
  end

  private

  ONE_HOUR = 1.hours

  def calculate_bill(parking_start_time)
    bill_amount = 0
    parked_time = (parking_start_time - Time.now.utc) / ONE_HOUR
    temp_parked_time = parked_time
    intl_hour_cost = 1
    begin
      bill_amount = bill_amount + intl_hour_cost
      intl_hour_cost = intl_hour_cost + 0.5
      temp_parked_time = temp_parked_time - 1
    end while(temp_parked_time > 0)
    bill_amount
  end

end
