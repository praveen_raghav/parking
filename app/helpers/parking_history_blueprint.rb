class ParkingHistoryBlueprint
  
  view :show_all_parking_status do
    field :id
    field :is_occupied
    field :space_type
  end
end