class ParkingSpace < ActiveRecord::Base

  PARKING_TYPE = {
      two_wheeler: 1,
      four_wheeler: 2,
      bus: 3
  }
end
